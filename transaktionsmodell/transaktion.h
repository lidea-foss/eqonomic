#ifndef TRANSAKTION_H
#define TRANSAKTION_H

#include <QDate>
#include <QObject>

class TransaktionsRad;
class Transaktion : public QObject
{
		Q_OBJECT

	public:
		typedef QList<TransaktionsRad *>	TransaktionsRader;

	public:
		explicit Transaktion(QObject *parent = nullptr);

		QDate datum() const;
		QString transaktionsText() const;
		QList<TransaktionsRad *> transaktionsRader() const;

		void rensaAllaTransaktionsRaderUtomForsta();

		QString	toString() const;



	public slots:

		void setDatum(const QDate &newDatum);
		void setTransaktionsText(const QString &newTransaktionsText);
		void addTransaktionsRad(TransaktionsRad *nyRad);
		void setTransaktionsRader(const QList<TransaktionsRad *> &newTransaktionsRader);
		void setTransaktionsRaderUtomForsta(const QList<TransaktionsRad *> &newTransaktionsRader);

	signals:
		void rowsChanged();
		void dateChanged(QDate newDate);
		void descriptionChanged(QString newHeading);


	private:
		QDate						mDatum;
		QString						mTransaktionsText;
		QList<TransaktionsRad*>		mTransaktionsRader;


};

#endif // TRANSAKTION_H
