#include "transaktionsraderwidget.h"
#include "transaktionsradwidget.h"

#include <transaktionsmodell/transaktion.h>
#include <kontomodell/kontoplanlistmodell.h>
#include <momsmodell/momskodlistmodell.h>

#include <QVBoxLayout>
#include <QtDebug>

TransaktionsRaderWidget::TransaktionsRaderWidget(QWidget *parent)
	: QWidget{parent}
{

}

void TransaktionsRaderWidget::setup(Transaktion *transaktion, KontoplanListmodell *kontoplan, MomskodListModell *momsplan)
{
	mTransaktion	= transaktion;
	mKontoplan		= kontoplan;
	mMomsplan		= momsplan;

	connect(mTransaktion, &Transaktion::rowsChanged, this, &TransaktionsRaderWidget::rowsChanged);

	rowsChanged();

}

void TransaktionsRaderWidget::rowsChanged()
{
	if (mTransaktion)
	{
		qDebug() << "Gör om verifikationslistan";
		QVBoxLayout* newLayout = new QVBoxLayout();

		// Töm den gamla layouten och skapa om hela listan.
		QLayout* oldLayout = layout();
		while(oldLayout && oldLayout->count())
		{
			QLayoutItem *widgetItem = oldLayout->takeAt(0);
			if (widgetItem)
			{
				QWidget *widget = widgetItem->widget();

				delete widgetItem;
				widget->deleteLater();
			}
		}
		delete oldLayout;


		newLayout->setSpacing(0);

		TransaktionsRadWidget* trans = nullptr;
		foreach(TransaktionsRad* rad, mTransaktion->transaktionsRader())
		{
			trans = new TransaktionsRadWidget(this);
			trans->setup(rad, mKontoplan);

			newLayout->addWidget(trans);

			qDebug() << "Adds a row";
		}
		if (trans)
		{
			trans->setIsLastRow(true);
			connect(trans, &TransaktionsRadWidget::addAnotherRow, this, &TransaktionsRaderWidget::anotherRowIsExpected);
		}

		setLayout(newLayout);
		updateGeometry();
	}

}

void TransaktionsRaderWidget::anotherRowIsExpected()
{
	mTransaktion->addTransaktionsRad(new TransaktionsRad());

	QLayout* currentLayout = layout();
	if (currentLayout && currentLayout->count())
	{
		QLayoutItem *widgetItem = currentLayout->itemAt(currentLayout->count() - 1);
		if (widgetItem)
		{
			widgetItem->widget()->setFocus();
		}

	}

}
