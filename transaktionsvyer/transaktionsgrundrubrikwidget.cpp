#include "transaktionsgrundrubrikwidget.h"
#include "ui_transaktionsgrundrubrikwidget.h"

TransaktionsGrundRubrikWidget::TransaktionsGrundRubrikWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::TransaktionsGrundRubrikWidget)
{
	ui->setupUi(this);
}

TransaktionsGrundRubrikWidget::~TransaktionsGrundRubrikWidget()
{
	delete ui;
}
