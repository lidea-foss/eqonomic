#ifndef SKAPATRANSAKTIONWIDGET_H
#define SKAPATRANSAKTIONWIDGET_H

#include <QWidget>

namespace Ui {
class SkapaTransaktionWidget;
}

class SkapaTransaktionWidget : public QWidget
{
		Q_OBJECT

	public:
		explicit SkapaTransaktionWidget(QWidget *parent = nullptr);
		~SkapaTransaktionWidget();

	private:
		Ui::SkapaTransaktionWidget *ui;
};

#endif // SKAPATRANSAKTIONWIDGET_H
