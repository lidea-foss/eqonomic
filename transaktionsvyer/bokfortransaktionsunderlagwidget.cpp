#include "bokfortransaktionsunderlagdelegate.h"
#include "bokfortransaktionsunderlagwidget.h"
#include "ui_bokfortransaktionsunderlagwidget.h"

#include <kontomodell/kontoplan.h>
#include <kontomodell/transaktionsunderlag.h>
#include <kontomodell/transaktionsunderlagtreemodel.h>

BokforTransaktionsUnderlagWidget::BokforTransaktionsUnderlagWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::BokforTransaktionsUnderlagWidget),
	delegate(nullptr),
	mKontoplan(nullptr)

{
	ui->setupUi(this);
}

BokforTransaktionsUnderlagWidget::~BokforTransaktionsUnderlagWidget()
{
	delete ui;
}

void BokforTransaktionsUnderlagWidget::setup(Kontoplan *kontoplan, TransaktionsUnderlag *underlag)
{
	ui->pUnderlagsTree->setModel(new TransaktionsUnderlagTreeModel(underlag));

	mKontoplan = kontoplan;
	delegate = new BokforTransaktionsUnderlagDelegate(mKontoplan);
	ui->pUnderlagsTree->setItemDelegate(delegate);
}
