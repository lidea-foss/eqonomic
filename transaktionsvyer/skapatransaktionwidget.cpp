#include "skapatransaktionwidget.h"
#include "ui_skapatransaktionwidget.h"

SkapaTransaktionWidget::SkapaTransaktionWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::SkapaTransaktionWidget)
{
	ui->setupUi(this);
}

SkapaTransaktionWidget::~SkapaTransaktionWidget()
{
	delete ui;
}
