#ifndef BOKFORTRANSAKTIONSUNDERLAGDELEGATE_H
#define BOKFORTRANSAKTIONSUNDERLAGDELEGATE_H

#include <QStyledItemDelegate>

class Kontoplan;
class KontoplanListmodell;
class BokforTransaktionsUnderlagDelegate : public QStyledItemDelegate
{
		Q_OBJECT
	public:
		BokforTransaktionsUnderlagDelegate(Kontoplan *kontoplan, QObject *parent = nullptr);

		QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

		void setEditorData(QWidget *editor, const QModelIndex &index) const override;
//		void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;

//		void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

	private:

		KontoplanListmodell*		mKontoplan;
		QPixmap						mMerKnappBild;
		QPixmap						mOkKnappBild;
};

#endif // BOKFORTRANSAKTIONSUNDERLAGDELEGATE_H
