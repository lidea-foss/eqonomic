#include "bokfortransaktionsunderlagdelegate.h"

#include <QComboBox>
#include <QDateEdit>
#include <QLineEdit>
#include <QPushButton>

#include <kontomodell/kontoplan.h>
#include <kontomodell/kontoplanlistmodell.h>

BokforTransaktionsUnderlagDelegate::BokforTransaktionsUnderlagDelegate(Kontoplan *kontoplan, QObject *parent)
	: QStyledItemDelegate(parent)
	, mKontoplan(nullptr)
{

	mKontoplan = new KontoplanListmodell(kontoplan);

//	QPushButton	okKnapp();
//	okKnapp.setText("Bokför");
//	mOkKnappBild = QPixmap(okKnapp.size());
//	okKnapp.render(&mOkKnappBild);

}

QWidget *BokforTransaktionsUnderlagDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option)

	QLineEdit*		lineEditor	= nullptr;
	QComboBox*		kontoLista	= nullptr;
	QPushButton*	knapp		= nullptr;

	if (index.isValid())
	{
		switch (index.column()) {
			case 0:
			case 2:
				lineEditor = new QLineEdit(parent);
				lineEditor->setText(index.data().toString());
				lineEditor->setReadOnly(true);
				return lineEditor;
				break;

			case 1:
				lineEditor = new QLineEdit(parent);
				lineEditor->setText(index.data().toString());
				return lineEditor;
				break;


			case 3:
				kontoLista = new QComboBox(parent);
				kontoLista->setModel(mKontoplan);
				return kontoLista;
				break;

			case 6:
				knapp = new QPushButton(parent);
				knapp->setText("...");
				return knapp;
				break;


			default:
				lineEditor = new QLineEdit(parent);
				return lineEditor;
				break;
		}
	}

    return nullptr;


}

void BokforTransaktionsUnderlagDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QDateEdit* dateEditor = nullptr;
    QLineEdit* lineEditor = nullptr;

	if (index.isValid())
	{
		switch (index.column()) {
			case 3:
				break;

			default:
				break;
		}
	}
}
