#ifndef BOKFORTRANSAKTIONSUNDERLAGWIDGET_H
#define BOKFORTRANSAKTIONSUNDERLAGWIDGET_H

#include <QWidget>

namespace Ui {
class BokforTransaktionsUnderlagWidget;
}

class Kontoplan;
class BokforTransaktionsUnderlagDelegate;
class TransaktionsUnderlag;
class BokforTransaktionsUnderlagWidget : public QWidget
{
		Q_OBJECT

	public:
		explicit BokforTransaktionsUnderlagWidget(QWidget *parent = nullptr);
		~BokforTransaktionsUnderlagWidget();

		void setup(Kontoplan* kontoplan, TransaktionsUnderlag* underlag);

	private:
		Ui::BokforTransaktionsUnderlagWidget	*ui;
		BokforTransaktionsUnderlagDelegate		*delegate;
		Kontoplan								*mKontoplan;
};

#endif // BOKFORTRANSAKTIONSUNDERLAGWIDGET_H
