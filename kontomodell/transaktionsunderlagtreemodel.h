#ifndef TRANSAKTIONSUNDERLAGTREEMODEL_H
#define TRANSAKTIONSUNDERLAGTREEMODEL_H

#include <QAbstractItemModel>
#include <QPixmap>
#include <QPointer>

class TransaktionsUnderlag;

class TransaktionsUnderlagTreeModel : public QAbstractItemModel
{
		Q_OBJECT

	public:
		explicit TransaktionsUnderlagTreeModel(TransaktionsUnderlag* underlag, QObject *parent = nullptr);

		// Header:
		QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

		// Basic functionality:
		QModelIndex index(int row, int column,
						  const QModelIndex &parent = QModelIndex()) const override;
		QModelIndex parent(const QModelIndex &index) const override;

		int rowCount(const QModelIndex &parent = QModelIndex()) const override;
		int columnCount(const QModelIndex &parent = QModelIndex()) const override;

		QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

		// Editable:
		bool setData(const QModelIndex &index, const QVariant &value,
					 int role = Qt::EditRole) override;

		Qt::ItemFlags flags(const QModelIndex& index) const override;

	private:

		QStringList							mRubriker;
		QPointer<TransaktionsUnderlag>		mUnderlag;
		QPixmap								mMerKnappBild;
		QPixmap								mOkKnappBild;

};

#endif // TRANSAKTIONSUNDERLAGTREEMODEL_H
