#ifndef TRANSAKTIONSUNDERLAG_H
#define TRANSAKTIONSUNDERLAG_H

#include <QObject>

class TransaktionsUnderlagRad;
class TransaktionsUnderlag : public QObject
{
		Q_OBJECT
	public:
		explicit TransaktionsUnderlag(QObject *parent = nullptr);

		void add(TransaktionsUnderlagRad* rad);
		void add(QList<TransaktionsUnderlagRad*> rader);

		TransaktionsUnderlagRad* radPerIndex(int index);
		int size() const;

	signals:
		void beginInsertRows(int first, int last);
		void endInsertRows();

	private:
		void insertSortedToList(TransaktionsUnderlagRad *rad);

	private:

		QList<TransaktionsUnderlagRad*>		mRader;

};

#endif // TRANSAKTIONSUNDERLAG_H
