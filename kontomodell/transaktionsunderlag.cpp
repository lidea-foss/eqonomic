#include "transaktionsunderlag.h"
#include "transaktionsunderlagrad.h"

TransaktionsUnderlag::TransaktionsUnderlag(QObject *parent)
	: QObject{parent}
{

}

void TransaktionsUnderlag::add(TransaktionsUnderlagRad *rad)
{
	if (rad)
		insertSortedToList(rad);

}

void TransaktionsUnderlag::add(QList<TransaktionsUnderlagRad *> rader)
{

	foreach(TransaktionsUnderlagRad *rad, rader)
	{
		insertSortedToList(rad);
	}
}

TransaktionsUnderlagRad *TransaktionsUnderlag::radPerIndex(int index)
{

	if (index >= 0 && index < size())
		return mRader.at(index);

	return nullptr;

}

int TransaktionsUnderlag::size() const
{
	return mRader.size();
}

void TransaktionsUnderlag::insertSortedToList(TransaktionsUnderlagRad *rad)
{
	if (rad)
	{

		bool bInserted = false;
		QList<TransaktionsUnderlagRad*>::iterator i;
		int index = 0;

		for (i = mRader.begin(); i != mRader.end() && !bInserted; i++, index++)
		{
			if (*i && (*i)->datum() > rad->datum())
			{
				beginInsertRows(index, index + 1);
				mRader.insert(i, rad);
				bInserted = true;
				endInsertRows();
				break;
			}
		}

		if (!bInserted)
		{
			beginInsertRows(mRader.size(), mRader.size());
			mRader.append(rad);
			endInsertRows();
		}

	}
}
