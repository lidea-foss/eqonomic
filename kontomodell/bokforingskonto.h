#ifndef BOKFORINGSKONTO_H
#define BOKFORINGSKONTO_H

#include <QObject>

class Bokforingskonto : public QObject
{
		Q_OBJECT

	public:
		static const int KontonummerNull = 0;
		typedef unsigned short T_KONTONUMMER;

        enum KontoTyp:int { OspecificeradTyp = 0,
                              Intakt,
                              Kostnad,
                              Skuld,
                              Tillgang };

		enum MomsStatus:int {OspecificeradMoms = 0,
							 IngenMoms,
							 KanHaMoms,
							 AlltidMoms,
							 LastMoms };

	public:
		explicit Bokforingskonto(QObject *parent = nullptr);
		explicit Bokforingskonto(T_KONTONUMMER kontoNummer, QString kontoNamn, QObject *parent = nullptr);
		explicit Bokforingskonto(const Bokforingskonto& other);

		T_KONTONUMMER kontoNummer() const;
		void setKontoNummer(T_KONTONUMMER newKontoNummer);

		QString kontoNamn() const;
		void setKontoNamn(const QString &newKontoNamn);

		QString toString() const;

		void reset();

		MomsStatus momsStatus() const;
		void setMomsStatus(MomsStatus newMomsStatus);

        QString momsKod() const;
        void setMomsKod(const QString& newMomsKod);

        KontoTyp kontoTyp() const;
        void setKontoTyp(KontoTyp newKontoTyp);

    signals:

	private:
		T_KONTONUMMER	mKontoNummer;
		QString			mKontoNamn;
        KontoTyp        mKontoTyp;
		MomsStatus		mMomsStatus;
        QString 		mMomsKod;

};

#endif // BOKFORINGSKONTO_H
