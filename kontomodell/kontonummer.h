#ifndef KONTONUMMER_H
#define KONTONUMMER_H

#include <QString>
#include <QVariant>


class KontoNummer
{
    public:
        explicit KontoNummer();
        explicit KontoNummer(const QVariant& kontonummer);
        explicit KontoNummer(const int kontonummer);
        explicit KontoNummer(const KontoNummer& other);

        ~KontoNummer();

        bool isValid() const;


        KontoNummer& operator = (const int kontonummer);
        KontoNummer& operator = (const QVariant &kontonummer);
        KontoNummer& operator = (const KontoNummer &kontonummer);

        bool operator == (const int kontonummer);
        bool operator == (const QVariant &kontonummer);
        bool operator == (const KontoNummer &kontonummer);


        operator QVariant () const;
        QVariant toVariant() const;

    protected:

        int     mNummer;

};

#endif // KONTONUMMER_H
