#include "transaktionsunderlag.h"
#include "transaktionsunderlagrad.h"
#include "transaktionsunderlagtreemodel.h"


#include <QPushButton>
#include <QSize>
#include <QtDebug>

TransaktionsUnderlagTreeModel::TransaktionsUnderlagTreeModel(TransaktionsUnderlag* underlag, QObject *parent)
	: QAbstractItemModel(parent)
	, mUnderlag(underlag)
{
	mRubriker << "Datum" << "Text & Mer info" << "Belopp" << "Konto" << "Moms" << "Mosbelopp" << "Mer" << "Bokför";

	QPushButton	okKnapp("Bokför");
	okKnapp.adjustSize();
	qDebug() << "Knappstorlek:" << okKnapp.size();
	mOkKnappBild = QPixmap(okKnapp.size());
	okKnapp.render(&mOkKnappBild);

	QPushButton	merKnapp("Mer");
	merKnapp.adjustSize();
	qDebug() << "Knappstorlek:" << merKnapp.size();
	mMerKnappBild = QPixmap(merKnapp.size());
	merKnapp.render(&mMerKnappBild);
}

QVariant TransaktionsUnderlagTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	Q_UNUSED(orientation)

	if (Qt::DisplayRole == role)
	{
		if (section >= 0 && section < mRubriker.size())
			return mRubriker.at(section);
	}
	else if (Qt::SizeHintRole == role)
	{
		switch(section)
		{
			case 0:
				return QSize(110, 22);
				break;

			case 1:
				return QSize(300, 22);
				break;

			case 2:
				return QSize(112, 22);
				break;

			case 3:
				return QSize(293, 22);
				break;

			case 4:
				return QSize(90, 22);
				break;

			case 5:
				return QSize(112, 22);
				break;

			case 6:
				return QSize(24, 22);
				break;

			case 7:
				return QSize(80, 22);
				break;

			default:
				return QVariant();
				break;
		}
	}
	return QVariant();

}

QModelIndex TransaktionsUnderlagTreeModel::index(int row, int column, const QModelIndex &parent) const
{
	if (!parent.isValid())
		return createIndex(row, column);

	return QModelIndex();
}

QModelIndex TransaktionsUnderlagTreeModel::parent(const QModelIndex &index) const
{
    Q_UNUSED(index);
	return QModelIndex();
}

int TransaktionsUnderlagTreeModel::rowCount(const QModelIndex &parent) const
{
	if (!parent.isValid())
		return mUnderlag->size();

	return 0;
}

int TransaktionsUnderlagTreeModel::columnCount(const QModelIndex &parent) const
{
	if (!parent.isValid())
		return 8;
    else
        return 0;

}

QVariant TransaktionsUnderlagTreeModel::data(const QModelIndex &index, int role) const
{
	TransaktionsUnderlagRad* rad = mUnderlag->radPerIndex(index.row());

	if (index.isValid() && role == Qt::DisplayRole && rad)
	{
		switch(index.column())
		{
			case 0:
				return rad->datum();
				break;

			case 1:
				return rad->info();
				break;

			case 2:
				return rad->belopp();
				break;


			default:
				return QVariant();
				break;
		}
	}
	else if (index.isValid() && role == Qt::DecorationRole && rad)
	{
		switch(index.column())
		{
			case 6:
				return mMerKnappBild;
				break;

			case 7:
				return mOkKnappBild;
				break;

			default:
				return QVariant();
				break;
		}

	}
	else if (index.isValid() && role == Qt::TextAlignmentRole && rad)
	{
		switch(index.column())
		{
			case 2:
			case 5:
				return Qt::AlignRight;
				break;

			default:
				return Qt::AlignLeft;
				break;
		}

	}

	return QVariant();
}

bool TransaktionsUnderlagTreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (data(index, role) != value) {
		// FIXME: Implement me!
		emit dataChanged(index, index, {role});
		return true;
	}
	return false;
}

Qt::ItemFlags TransaktionsUnderlagTreeModel::flags(const QModelIndex &index) const
{
	Qt::ItemFlags flags = Qt::NoItemFlags;

	if (index.isValid())
	{
		flags |= Qt::ItemIsSelectable;
		flags |= Qt::ItemIsEnabled;
		flags |= Qt::ItemNeverHasChildren;

		if (index.row() == 0 || index.row() == 1 || index.row() == 3)
			flags |= Qt::ItemIsEditable;

	}


	return flags; // FIXME: Implement me!
}


