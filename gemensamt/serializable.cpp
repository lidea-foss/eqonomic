#include <exceptions/objectinitializationfailedexception.h>
#include "serializable.h"

#include <QDir>
#include <QFile>

#include <QtDebug>

#include <exceptions/failedtoreadjsonfileexception.h>
#include <exceptions/fileerrorexception.h>

void Serializable::saveJsonDocument(QJsonDocument jsonDoc, const QString &filePath, const QString &context)
{
    qInfo().noquote() << "Sparar fil för " << context << " under " << filePath;


    QFile file(filePath);
    QFileInfo fileInfo(file);

    if (!fileInfo.dir().mkpath("."))
    {
        qCritical() << "Kunde inte skapa vald folder för skrivning.";
        throw FileErrorException(fileInfo.path(), "skrivning");
    }


    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "Filen är öppen, skrivning sker";
        file.write(jsonDoc.toJson(QJsonDocument::Indented));
        file.close();
    }
    else
    {
        qCritical() << "Kunde inte öppna filen för skrivning";
        throw FileErrorException(file, "skrivning");
    }

}

Serializable::Serializable()
{

}

Serializable::Serializable(const QString &filePath)
    : mFilePath{filePath}
{

}

QString Serializable::filePath() const
{
    return mFilePath;
}

QJsonDocument Serializable::readJsonDocument(const QString &context)
{
    QJsonDocument jsonDoc;

    qDebug().noquote() << "Öppnar fil med " << context << " under " << this->filePath();

    QFile file(this->filePath());

    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "Filen är öppen, läsning sker";
        QJsonParseError error;
        jsonDoc = QJsonDocument::fromJson(file.readAll(), &error);

        if (error.error != QJsonParseError::NoError)
        {
            qCritical() << "Misslyckades att läsa filen";
            qCritical() << "Felkod  : " << error.error;
            qCritical() << "Felinfo : " << error.errorString();
            qCritical() << "Offset  : " << error.offset;
            qCritical() << "Throws an FailedToReadJSONFileException with this info. ";

            FailedToReadJSONFileException e(context, error);
            throw (e);

        }

        file.close();
    }
    else
    {
        qCritical() << "Kunde inte öppna filen för läsning";
        qCritical() << "Throws an FileErrorException with this info. ";
        throw FileErrorException(file, "läsning");

    }

    return jsonDoc;

}
