#ifndef SERIALIZABLE_H
#define SERIALIZABLE_H

#include <QVariantHash>
#include <QJsonDocument>
#include <QString>

class Serializable
{
    public:

        Serializable();
        explicit Serializable(const QString& filePath);
        virtual ~Serializable() {}

        virtual QVariantHash serialize() const = 0;
        virtual void deserialize(QVariantHash object) = 0;

        virtual QString filePath() const;

    protected:
        // Helperfunctions

        QJsonDocument readJsonDocument(const QString& context = QString());

        static void saveJsonDocument(QJsonDocument jsonDoc, const QString& filePath, const QString& context = QString());


    protected:
        QString         mFilePath;

};

#endif // SERIALIZABLE_H
