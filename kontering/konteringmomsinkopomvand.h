#ifndef KONTERINGMOMSINKOPOMVAND_H
#define KONTERINGMOMSINKOPOMVAND_H

#include "konteringsmallmoms.h"

#include <kontomodell/kontonummer.h>

class KonteringMomsInkopOmvand : public KonteringsmallMoms
{
        Q_OBJECT

    public:
        explicit KonteringMomsInkopOmvand(QVariantHash object, QObject *parent = nullptr);
        virtual QVariantHash serialize() const override;
        void deserialize(QVariantHash object) override;
        static bool isThisObject(QVariantHash object);


    protected:
        KontoNummer     mIngaendeMomskonto;
        KontoNummer     mUtgaendeMomskonto;

};

#endif // KONTERINGMOMSINKOPOMVAND_H
