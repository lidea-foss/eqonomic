#ifndef KONTERINGMOMSFORSALJNING_H
#define KONTERINGMOMSFORSALJNING_H

#include "konteringsmallmoms.h"

#include <kontomodell/kontonummer.h>

class KonteringMomsForsaljning : public KonteringsmallMoms
{
        Q_OBJECT
    public:
        explicit KonteringMomsForsaljning(QVariantHash object, QObject *parent = nullptr);
        virtual QVariantHash serialize() const override;
        void deserialize(QVariantHash object) override;
        static bool isThisObject(QVariantHash object);

    protected:
        KontoNummer     mMomskonto;

};

#endif // KONTERINGMOMSFORSALJNING_H
