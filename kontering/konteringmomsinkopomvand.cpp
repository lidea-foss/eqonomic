#include "konteringmomsinkopomvand.h"

#include <exceptions/objectinitializationfailedexception.h>

KonteringMomsInkopOmvand::KonteringMomsInkopOmvand(QVariantHash object, QObject *parent)
    : KonteringsmallMoms{parent}
{
    deserialize(object);
}

QVariantHash KonteringMomsInkopOmvand::serialize() const
{
    QVariantHash result = KonteringsmallMoms::serialize();

    result.insert("Mall", "Omvänd");
    result.insert("Ingående", mIngaendeMomskonto);
    result.insert("Utgående", mUtgaendeMomskonto);

    return result;

}

void KonteringMomsInkopOmvand::deserialize(QVariantHash object)
{
    if (isThisObject(object))
    {
        KonteringsmallMoms::deserialize(object);

        mIngaendeMomskonto = KontoNummer(object.value("Ingående"));
        mUtgaendeMomskonto = KontoNummer(object.value("Utgående"));

        if (!mIngaendeMomskonto.isValid() || !mUtgaendeMomskonto.isValid())
            throw ObjectInitializationFailedException("Fel i underlaget för att skapa en KonteringMomsInkopOmvand",
                                                      "KonteringMomsInkopOmvand::deserialize(QVariantHash object)");
    }
    else
        throw ObjectInitializationFailedException("Fel underlag för att skapa en KonteringMomsInkopOmvand",
                                                  "KonteringMomsInkopOmvand::deserialize(QVariantHash object)");
}

bool KonteringMomsInkopOmvand::isThisObject(QVariantHash object)
{
    if ( object.value("Mall") == "Omvänd"
         && object.contains("Ingående")
         && object.contains("Utgående"))

        return true;

    return false;
}
