#include "konteringmomsinkop.h"

#include <QVariant>

#include <exceptions/objectinitializationfailedexception.h>

KonteringMomsInkop::KonteringMomsInkop(QVariantHash object, QObject *parent)
    : KonteringsmallMoms{parent}
{
    deserialize(object);
}

QVariantHash KonteringMomsInkop::serialize() const
{
    QVariantHash result = KonteringsmallMoms::serialize();

    result.insert("Mall", "Inköp");
    result.insert("Ingående", mMomskonto);

    return result;

}

void KonteringMomsInkop::deserialize(QVariantHash object)
{
    if (isThisObject(object))
    {
        KonteringsmallMoms::deserialize(object);

        mMomskonto = KontoNummer(object.value("Ingående"));

        if (!mMomskonto.isValid())
            throw ObjectInitializationFailedException("Fel i underlaget för att skapa en KonteringMomsInkop",
                                                      "KonteringMomsInkop::deserialize(QVariantHash object)");
    }
    else
        throw ObjectInitializationFailedException("Fel underlag för att skapa en KonteringMomsInkop",
                                                  "KonteringMomsInkop::deserialize(QVariantHash object)");

}

bool KonteringMomsInkop::isThisObject(QVariantHash object)
{
    if (object.value("Mall") == "Inköp"
            && object.contains("Ingående"))

        return true;

    return false;
}

