#ifndef FAILEDTOREADJSONFILEEXCEPTION_H
#define FAILEDTOREADJSONFILEEXCEPTION_H

#include "commonexception.h"
#include <QJsonParseError>

class FailedToReadJSONFileException : public CommonException
{
    public:
        FailedToReadJSONFileException();
        FailedToReadJSONFileException(const QString& sContext, const QJsonParseError& pError);
        FailedToReadJSONFileException(const FailedToReadJSONFileException& other);

        virtual ~FailedToReadJSONFileException() {}

        virtual QException* clone() const override;
        virtual void raise() const override;

        virtual QString exceptionType() const override;
        virtual QString summary() const override;
        virtual QString details() const override;


    public:
        QString                         context;
        QString                         errorMessage;
        QJsonParseError::ParseError     error;
        int                             offset;
};

#endif // FAILEDTOREADJSONFILEEXCEPTION_H
