#include "fileerrorexception.h"

FileErrorException::FileErrorException(const QFile &aFile, const QString &anIntendedOperation)
    : fileName{ aFile.fileName()}
    , intendedOperation { anIntendedOperation }
    , errorString {aFile.errorString() }
    , errorCode {aFile.error() }
{

}

FileErrorException::FileErrorException(const FileErrorException &other)
    : fileName{ other.fileName}
    , intendedOperation { other.intendedOperation }
{

}

QException *FileErrorException::clone() const
{
    return new FileErrorException(*this);
}

void FileErrorException::raise() const
{
    throw *this;
}

QString FileErrorException::exceptionType() const
{
    return "FileErrorException";
}

QString FileErrorException::summary() const
{
    return QString("Kunde inte öppna filen %1 för %2.").arg(fileName).arg(intendedOperation);
}

QString FileErrorException::details() const
{
    return QString("Felkoden är %1 (%2).")
            .arg(errorCode)
            .arg(errorString);
}






