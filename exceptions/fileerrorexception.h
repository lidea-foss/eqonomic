#ifndef FILEERROREXCEPTION_H
#define FILEERROREXCEPTION_H

#include <QFile>

#include "commonexception.h"

class FileErrorException : public CommonException
{
    public:
        FileErrorException(const QFile& aFile, const QString& anIntendedOperation);
        FileErrorException(const FileErrorException& other);

        virtual QException* clone() const override;
        virtual void raise() const override;

        virtual QString exceptionType() const override;
        virtual QString summary() const override;
        virtual QString details() const override;

    public:
        QString                 fileName;
        QString                 intendedOperation;
        QString                 errorString;
        QFileDevice::FileError  errorCode;

};

#endif // FILEERROREXCEPTION_H
