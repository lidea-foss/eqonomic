#ifndef IMPORTKONTOPLANCSV_H
#define IMPORTKONTOPLANCSV_H

#include <QObject>

class Bokforingskonto;

class ImportKontoplanCSV : public QObject
{
		Q_OBJECT
	public:
		explicit ImportKontoplanCSV(QObject *parent = nullptr);

		QList<Bokforingskonto*> importera(const QString& importFileName);

    protected:
        bool setKontoTyp(Bokforingskonto* konto, QStringList &line);
        bool setMomsStatus(Bokforingskonto* konto, QStringList &line);
        bool setMomsKod(Bokforingskonto* konto, QStringList &line);

	signals:

};

#endif // IMPORTKONTOPLANCSV_H
