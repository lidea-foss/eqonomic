#ifndef IMPORTKONTOUTDRAGCSV_H
#define IMPORTKONTOUTDRAGCSV_H

#include <QObject>

class TransaktionsUnderlagRad;
class ImportKontoutdragCSV : public QObject
{
		Q_OBJECT
	public:
		explicit ImportKontoutdragCSV(QObject *parent = nullptr);		
		QList<TransaktionsUnderlagRad*> importera(const QString& importFileName);

	signals:

};

#endif // IMPORTKONTOUTDRAGCSV_H
