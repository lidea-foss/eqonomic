#include "importkontoutdragcsv.h"

#include <kontomodell/transaktionsunderlagrad.h>

#include <QFile>
#include <QTextStream>


#include <QtDebug>

ImportKontoutdragCSV::ImportKontoutdragCSV(QObject *parent)
	: QObject{parent}
{

}


// Just nu hårdkodat mot Swedbanks CSV-fil.
QList<TransaktionsUnderlagRad *> ImportKontoutdragCSV::importera(const QString &importFileName)
{
	QList<TransaktionsUnderlagRad *>	result;
	QFile							source(importFileName);
	int								skipRows = 2;

	if (source.open(QIODevice::Text  | QIODevice::ReadOnly))
	{
		QTextStream in(&source);
		in.setCodec("ISO-8859-15");

		while(!in.atEnd())
		{
			QStringList line = in.readLine().split(",");

			if (skipRows > 0)
			{
				qDebug() << "Skippar rad: " << line;
				skipRows--;
			}
			else
			{
				qDebug() << "Tolkar rad: " << line;

				{
					if (line.size() == 12)
					{
						bool bOk = true;

						QDate								datum;
						QString								info;
						qreal								belopp;

						belopp = line[10].toDouble(&bOk);
						if (bOk)
						{
							datum = QDate::fromString(line[5], Qt::ISODate);
							info = line[8];
							info.remove('\"');

							TransaktionsUnderlagRad* underlag = new TransaktionsUnderlagRad(datum, info, belopp, 1930);
							result.prepend(underlag);
						}

					}
				}
			}
		}

		qDebug() << "Import klar, importerade " << result.size() << " transaktioner.";
	}

	return result;
}
