#include "momskodlistmodell.h"

#include "momskoder.h"
#include "momskod.h"

MomskodListModell::MomskodListModell(Momskoder *momsKoder, Momskoder::Momstyp momstyp, QObject *parent)
	: QAbstractListModel(parent)
    , mMomstyp(momstyp)
	, mMomskoder(momsKoder)
{
}

Momskoder::Momstyp MomskodListModell::momstyp() const
{
    return mMomstyp;
}

QVariant MomskodListModell::headerData(int section, Qt::Orientation orientation, int role) const
{
	Q_UNUSED(orientation)
	Q_UNUSED(role);

	if (section == 0 && role == Qt::DisplayRole)
		return QString("Moms");

	else return QVariant();
}

int MomskodListModell::rowCount(const QModelIndex &parent) const
{
	// For list models only the root node (an invalid parent) should return the list's size. For all
	// other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
	if (!parent.isValid() && mMomskoder)
        return mMomskoder->size(mMomstyp);

	return 0;

}

QVariant MomskodListModell::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

    if (role == Qt::DisplayRole && mMomskoder->momskodPerIndex(mMomstyp, index.row()))
	{
        return mMomskoder->momskodPerIndex(mMomstyp, index.row())->toString();
	}

	return QVariant();
}

int MomskodListModell::indexForMomskod(const QString& momsKod)
{
	if (mMomskoder)
        return mMomskoder->indexForMomskod(mMomstyp, momsKod);
    else
        return -1;

}

Momskod *MomskodListModell::momskodPerIndex(int index)
{
	if (mMomskoder)
        return mMomskoder->momskodPerIndex(mMomstyp, index);
    else
        return nullptr;

}

QPointer<Momskoder> MomskodListModell::momskoder() const
{
	return mMomskoder;
}
