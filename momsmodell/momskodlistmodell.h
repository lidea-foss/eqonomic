#ifndef MOMSKODLISTMODELL_H
#define MOMSKODLISTMODELL_H

#include <QAbstractListModel>
#include <QPointer>

#include "momskoder.h"

class Momskod;
class MomskodListModell : public QAbstractListModel
{
		Q_OBJECT

	public:
        explicit MomskodListModell(Momskoder* momsKoder, Momskoder::Momstyp momstyp, QObject *parent = nullptr);

        Momskoder::Momstyp momstyp() const;

		// Header:
		QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

		// Basic functionality:
		int rowCount(const QModelIndex &parent = QModelIndex()) const override;

		QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

        int indexForMomskod(const QString &momsKod);
		Momskod* momskodPerIndex(int index);

		QPointer<Momskoder> momskoder() const;

	private:

        Momskoder::Momstyp          mMomstyp;
		QPointer<Momskoder>			mMomskoder;


};

#endif // MOMSKODLISTMODELL_H
