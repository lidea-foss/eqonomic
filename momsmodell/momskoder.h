#ifndef MOMSKODER_H
#define MOMSKODER_H

#include <QObject>
#include <kontomodell/bokforingskonto.h>
#include <kontomodell/kontonummer.h>
#include <gemensamt/serializable.h>

class Momskod;
class Momskoder : public QObject, public Serializable
{
		Q_OBJECT

    public:

        enum Momstyp:int { Undefined = 0,
                           Inkop,
                           Forsaljning };

    public:
		explicit Momskoder(QObject *parent = nullptr);
        explicit Momskoder(const QString& filePath, QObject *parent = nullptr);

		Bokforingskonto::T_KONTONUMMER		motkontoForMoms() const;

        int size(const Momstyp momstyp) const;
        Momskod* momskodPerIndex(const Momstyp momstyp, int index);
        Momskod* momskodPerKod(const Momstyp momstyp, const QString& momsKod);
        Momskod* momskodPerKod(const QString& momsKod);
        int		 indexForMomskod(const Momstyp momstyp, const QString& momskod);

        bool								harMoms(const QString &momsKod);
        qreal								momsSats(const QString &momsKod);
        Bokforingskonto::T_KONTONUMMER		momsKonto(const QString &momsKod);

        // Serialisable
        QVariantHash serialize() const override;
        virtual void deserialize(QVariantHash object) override;
        static bool isThisObject(QVariantHash object);

    public slots:
        void saveTo(const QString& filePath);

    protected:
        QList<Momskod* > modell(const Momstyp momstyp) const;


	private:
        KontoNummer             mRedovisningskontoMoms;
		QList<Momskod* >		mMomskoder;
        QList<Momskod* >        mMomskoderForsaljning;


};

#endif // MOMSKODER_H
