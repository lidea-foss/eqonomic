#include "kontoplanwidget.h"
#include "ui_kontoplanwidget.h"

#include <kontomodell/kontoplan.h>
#include <kontomodell/kontoplantreemodell.h>

KontoPlanWidget::KontoPlanWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::KontoPlanWidget)
{
	ui->setupUi(this);
}

KontoPlanWidget::~KontoPlanWidget()
{
	delete ui;
}

void KontoPlanWidget::setup(KontoplanTreemodell *treeModel)
{
	ui->pKontovy->setModel(treeModel);
}
