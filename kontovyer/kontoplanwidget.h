#ifndef KONTOPLANWIDGET_H
#define KONTOPLANWIDGET_H

#include <QPointer>
#include <QWidget>

namespace Ui {
class KontoPlanWidget;
}

class KontoplanTreemodell;
class Kontoplan;

class KontoPlanWidget : public QWidget
{
		Q_OBJECT

	public:
		explicit KontoPlanWidget(QWidget *parent = nullptr);
		~KontoPlanWidget();

		void setup(KontoplanTreemodell* treeModel);

	private:
		Ui::KontoPlanWidget *ui;

};

#endif // KONTOPLANWIDGET_H
