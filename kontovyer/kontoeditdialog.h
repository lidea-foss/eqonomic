#ifndef KONTOEDITDIALOG_H
#define KONTOEDITDIALOG_H

#include <QDialog>

#include <kontomodell/bokforingskonto.h>

namespace Ui {
class KontoEditDialog;
}

class QAbstractButton;
class Bokforingskonto;
class KontoEditDialog : public QDialog
{
		Q_OBJECT

	public:
		explicit KontoEditDialog(QWidget *parent = nullptr);
		~KontoEditDialog();

	signals:
		void nyttKonto(Bokforingskonto* konto);

	protected slots:
		void btnAcceptPressed();

	private:
		Ui::KontoEditDialog *ui;
		Bokforingskonto		mKonto;
};

#endif // KONTOEDITDIALOG_H
