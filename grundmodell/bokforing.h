#ifndef BOKFORING_H
#define BOKFORING_H

#include <QDir>
#include <QObject>
#include <QPointer>
#include <gemensamt/serializable.h>


class Organisation;
class BokforingsAr;
class Momskoder;
class Kontoplan;

class Bokforing : public QObject, public Serializable
{
        Q_OBJECT

    public:
        explicit Bokforing(QVariantHash object, const QString& basePath, QObject *parent = nullptr);
        explicit Bokforing(const QString& basePath, QObject *parent = nullptr);

        Momskoder* momskoder() const;
        Kontoplan* kontoplan() const;

        static bool isThisObject(QVariantHash object);
        virtual QVariantHash serialize() const override;
        void deserialize(QVariantHash object) override;

    public slots:

        void save();
        void saveTo(const QString& basePath);

    signals:


    protected:
        QDir                                    mBasePath;
        QPointer<Organisation>                  mOrgansation;
        QHash<int, QPointer<BokforingsAr> >     mBokforingsAr;
        QPointer<BokforingsAr>                  mAktuelltBokforingsAr;

};

#endif // BOKFORING_H
