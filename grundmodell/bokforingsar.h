#ifndef BOKFORINGSAR_H
#define BOKFORINGSAR_H

#include <QDir>
#include <QObject>
#include <QPointer>
#include <gemensamt/serializable.h>


class Momskoder;
class Kontoplan;

class BokforingsAr : public QObject, public Serializable
{
        Q_OBJECT

    public:

        enum Status:int {Undefined = 0,
                         Oppet,             // Året är fortfarande öppet för ändringar
                         Avslutat           // Året är avslutat och stängt för ändringar
                        };

        static const QMap<int, QString> StatusS;



        Q_ENUM(Status)

    public:
        explicit BokforingsAr(QVariantHash object, const QString& basePath, QObject *parent = nullptr);
        explicit BokforingsAr(const QString& basePath, QObject *parent = nullptr);

        static bool isThisObject(QVariantHash object);
        virtual QVariantHash serialize() const override;
        void deserialize(QVariantHash object) override;


        static QString StatusToString(Status status);
        static Status  StringToStatus(const QString& status, bool* bOk = nullptr);

        int ar() const;

        Momskoder* momskoder() const;
        Kontoplan* kontoplan() const;

    public slots:
        void saveTo(const QString& basePath);

    signals:

    protected:
        void laddaMomskoder();
        void laddaKontoplan();

    protected:

        QDir                    mBasePath;          // Basepath to this year

        int                     mAr;
        Status                  mStatus;
        bool                    bAktuellt;          // True om detta är det aktuella året som man jobbar med

        QPointer<Momskoder>     mMomskoder;
        QPointer<Kontoplan>     mKontoplan;


};

#endif // BOKFORINGSAR_H
