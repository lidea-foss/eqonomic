#include "organisation.h"

#include <exceptions/objectinitializationfailedexception.h>
#include <QDir>

Organisation::Organisation(QVariantHash object, QObject *parent)
    : QObject{parent}
{
    deserialize(object);
}

Organisation::Organisation(const QString &filePath, QObject *parent)
    : QObject{parent}
    , Serializable{filePath}
{
    deserialize(readJsonDocument("Organisation").toVariant().toHash());
}

bool Organisation::isThisObject(QVariantHash object)
{
    if (object.value("Object") == "Organisation")
        return true;

    return false;

}

QVariantHash Organisation::serialize() const
{
    QVariantHash result;

    result.insert("Object", "Organisation");
    result.insert("Namn", mNamn);

    return result;
}

void Organisation::deserialize(QVariantHash object)
{
    if (isThisObject(object))
    {
        mNamn = object.value("Namn").toString();
        return;

    }

    throw ObjectInitializationFailedException("Fel underlag för att skapa en Organisation.",
                                              "Organisation");

}

void Organisation::saveTo(const QString &filePath)
{
    qDebug() << "Sparar Organisation under " << filePath;

    saveJsonDocument(QJsonDocument::fromVariant(serialize()),
                     filePath,
                     "Organisation");

}

