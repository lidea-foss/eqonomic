#include "bokforingsar.h"

#include <QVariantHash>
#include <QMetaEnum>

#include <exceptions/objectinitializationfailedexception.h>
#include <import/kontoplan-csv/importkontoplancsv.h>
#include <kontomodell/kontoplan.h>
#include <momsmodell/momskoder.h>

#include <QJsonDocument>
#include <QtDebug>

const QMap<int, QString> BokforingsAr::StatusS(
        {
            {BokforingsAr::Undefined, "Undefined"},
            {BokforingsAr::Oppet, "Öppet"},
            {BokforingsAr::Avslutat, "Avslutat"}
        });


BokforingsAr::BokforingsAr(QVariantHash object, const QString &basePath, QObject *parent)
    : QObject{parent}
    , Serializable{QDir(basePath).filePath("Inställningar/Inställningar.json") }
    , mBasePath{basePath}
    , mStatus {Undefined}

{

    deserialize(object);
    laddaMomskoder();
    laddaKontoplan();

}

BokforingsAr::BokforingsAr(const QString &basePath, QObject *parent)
    : QObject{parent}
    , Serializable{QDir(basePath).filePath("Inställningar/Inställningar.json") }
    , mBasePath{basePath}
    , mStatus {Undefined}
{
    deserialize(readJsonDocument("Bokföringsår").toVariant().toHash());
    laddaMomskoder();
    laddaKontoplan();
}

QVariantHash BokforingsAr::serialize() const
{
    QVariantHash    result;

    result.insert("Object", "Bokföringsår");
    result.insert("Bokföringsår", mAr);

    result.insert("Status", StatusToString(mStatus));

    return result;


}

void BokforingsAr::deserialize(QVariantHash object)
{
    bool bOk = true;
    bool bOk2 = true;

    if (isThisObject(object))
    {
        mAr         = object.value("Bokföringsår").toInt(&bOk);
        mStatus     = StringToStatus(object.value("Status").toString(), &bOk2);
    }
    else
        throw ObjectInitializationFailedException("Fel underlag för att skapa ett Bokföringsår",
                                                  "BokforingsAr::deserialize(QVariantHash object)");

    if (!bOk || !bOk2 || mStatus == Undefined)
        throw ObjectInitializationFailedException("Fel i underlaget för att skapa ett Bokföringsår",
                                                  "BokforingsAr::deserialize(QVariantHash object)");

}

bool BokforingsAr::isThisObject(QVariantHash object)
{

    if ( object.value("Object") == "Bokföringsår")
        return true;

    return false;

}

QString BokforingsAr::StatusToString(Status status)
{
    return StatusS.value(status);
}

BokforingsAr::Status BokforingsAr::StringToStatus(const QString& status, bool* bOk)
{

    if (StatusS.values().contains(status))
        return static_cast<BokforingsAr::Status>(StatusS.key(status));
    else
    {
        if (bOk != nullptr)
        {
            *bOk = false;
            return Undefined;
        }

        throw ObjectInitializationFailedException(QString("Kan inte konvertera \"%1\" till Status").arg(status),
                                              "Bokföringsår");
    }

}

void BokforingsAr::laddaMomskoder()
{
    mMomskoder = new Momskoder(mBasePath.filePath("Inställningar/Momskoder.json"), this);
}


void BokforingsAr::laddaKontoplan()
{
    mKontoplan = new Kontoplan(this);

    ImportKontoplanCSV	import;
    QList<Bokforingskonto *> importList = import.importera("data/underlag/Kontoplan-2023 - K2 Enkel v0.1.csv");
//    QList<Bokforingskonto *> importList = import.importera("underlag/Felsök-momskod.csv");


    mKontoplan->importera(importList);


}

Kontoplan* BokforingsAr::kontoplan() const
{
    return mKontoplan;
}

void BokforingsAr::saveTo(const QString &basePath)
{

    qDebug() << "Sparar bokföring under " << basePath;

    saveJsonDocument(QJsonDocument::fromVariant(serialize()),
                     QDir(basePath).filePath("Inställningar/Bokföring.json"),
                     "Bokföringsår");

    mMomskoder->saveTo(QDir(basePath).filePath("Inställningar/Momskoder.json"));

}

Momskoder* BokforingsAr::momskoder() const
{
    return mMomskoder;
}

int BokforingsAr::ar() const
{
    return mAr;
}
